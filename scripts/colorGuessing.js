/*************************VARIABLES******************************/
let colorArray = [];
let currentWinner;
let isGameOver;
let bodyProps;
let currentMode;
let selected;
let pTip;

const gameMode = {
    EASY: 'easy',
    HARD: 'hard'
};
/*************************OBJECTS********************************/
function Color(red, green, blue, container) {
    this.r = red,
        this.g = green,
        this.b = blue,
        this.rGuess = 0,
        this.gGuess = 0,
        this.bGuess = 0,
        this.done = false,
        this.container = container;
    this.addEvent = function () {
        container.addEventListener('click', () => {
                if (currentMode === gameMode.EASY) {
                    isGameOver = (this.r === currentWinner.r && this.g === currentWinner.g && this.b === currentWinner.b);
                    checkIfGameOver(container);
                } else {
                    if (selected) {
                        selected.container.classList.remove("currentSelection");
                    }
                    selected = this;
                    this.container.classList.add("currentSelection");
                    changeLabel(this.rGuess, this.gGuess, this.bGuess);

                    if (selected && this.done === false) {
                        if(Number(rLabel.textContent)!==selected.r)
                        {
                            rLabel.style.color = '#0275d8';
                            rLabel.setAttribute('contenteditable', 'true');
                        }

                        if(Number(gLabel.textContent)!==selected.g)
                        {
                            gLabel.style.color = '#0275d8';
                            gLabel.setAttribute('contenteditable', 'true');
                        }

                        if(Number(bLabel.textContent)!==selected.b)
                        {
                            bLabel.style.color = '#0275d8';
                            bLabel.setAttribute('contenteditable', 'true');
                        }
                    }
                        if(Number(rLabel.textContent)===selected.r)
                        {

                            rLabel.style.color = 'red';
                            rLabel.setAttribute('contenteditable', 'false');
                        }
                        if(Number(gLabel.textContent)===selected.g)
                        {

                            gLabel.style.color = 'green';
                            gLabel.setAttribute('contenteditable', 'false');
                        }
                        if(Number(bLabel.textContent)===selected.b)
                        {

                            bLabel.style.color = 'blue';
                            bLabel.setAttribute('contenteditable', 'false');
                        }

                }
            }
        )
    }
}

/*************************BUTTONS*******************************/
let btnReset, btnEasy, btnHard;
btnReset = document.getElementById("btnReset");
btnEasy = document.getElementById("btnEasy");
btnHard = document.getElementById("btnHard");
btnEasy.classList.add('selected');
btnReset.addEventListener('click', () => newGame());
btnEasy.addEventListener('click', () => {
    btnEasy.classList.add('selected');
    btnHard.classList.remove('selected');
    currentMode = gameMode.EASY;
    newGame();

});
btnHard.addEventListener('click', () => {
    btnHard.classList.add('selected');
    btnEasy.classList.remove('selected');
    currentMode = gameMode.HARD;
    newGame();

});


/*************************LABELS********************************/
let rLabel, gLabel, bLabel;
rLabel = document.getElementById("rLabel");
gLabel = document.getElementById("gLabel");
bLabel = document.getElementById("bLabel");
bodyProps = document.getElementsByTagName("body")[0];
pTip = document.getElementById("guessTip");
/****************************MAIN*****************************/
currentMode = gameMode.EASY;
newGame();

/************************FUNCTIONS****************************/

function newGame() {
    resetContainers();
    generateColors();

    isGameOver = false;
    checkMode();
}

function generateColors() {
    if (colorArray.length === 0) {
        for (i = 0; i <= 5; i++) {//Creates container with  random color.
            colorArray.push(new Color(Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255),
                document.getElementById("colorOption" + (i))
            ));
            colorArray[i].addEvent();
            colorArray[i].container.style.background = `rgb(${colorArray[i].r},${colorArray[i].g},${colorArray[i].b}`;

        }
    } else {
        for (let i = 0; i < colorArray.length; i++) {//Generates random color for each container if the container is created already
            colorArray[i].r = Math.floor(Math.random() * 256);
            colorArray[i].g = Math.floor(Math.random() * 256);
            colorArray[i].b = Math.floor(Math.random() * 256);
            colorArray[i].container.style.background = `rgb(${colorArray[i].r},${colorArray[i].g},${colorArray[i].b}`;
        }
    }
}

function resetContainers() {
    bodyProps.classList.add("bg-dark");
    bodyProps.classList.remove("win");
    if (selected) {
        selected.container.classList.remove("currentSelection");
        selected=null;
    }
    changeLabel(0, 0, 0);
    colorArray.forEach((color) => {
            color.container.classList.contains("wrong") ? color.container.classList.remove("wrong") : null;
        rLabel.setAttribute('contenteditable', 'false');
        gLabel.setAttribute('contenteditable', 'false');
        bLabel.setAttribute('contenteditable', 'false');
        }
    )
}

function getWinner() {

    currentWinner = colorArray[Math.floor(Math.random() * colorArray.length)];
    changeLabel(currentWinner.r, currentWinner.g, currentWinner.b)

}

function startHard() {
 resetContainers();
    rLabel.addEventListener('keypress', (e) => {
        checkLabelInput(e)
    });
    rLabel.addEventListener('keyup', () => {
        selected.rGuess = Number(rLabel.textContent);
        if (Number(rLabel.textContent) === selected.r) {
            rLabel.setAttribute('contenteditable', 'false');
            rLabel.style.color = 'red';
        }
        else{
            createTip();
        }
    });

    gLabel.addEventListener('keypress', (e) => {
        checkLabelInput(e)
    });
    gLabel.addEventListener('keyup', () => {
        selected.gGuess = Number(gLabel.textContent);
        if (Number(gLabel.textContent) === selected.g) {

            gLabel.setAttribute('contenteditable', 'false');
            gLabel.style.color = 'green';
        }
        else{
            createTip();
        }
    });

    bLabel.addEventListener('keypress', (e) => {
        checkLabelInput(e)
    });
    bLabel.addEventListener('keyup', () => {
        selected.bGuess = Number(bLabel.textContent);

        if (Number(bLabel.textContent) === selected.b) {
            selected.bGuess=Number(bLabel.textContent);
            bLabel.setAttribute('contenteditable', 'false');
            bLabel.style.color = 'blue';
        }
        else{
            createTip();
        }
    });


    function checkLabelInput(e) {
        if (checkIntegerValue(e)) {
            e.preventDefault();
        } else {
            if (selected && !selected.done) {

                getGuessValues();

            }
        }




        function checkIntegerValue(e) {
            return (isNaN(Number(e.key)) || e.target.textContent.length >= 3 || checkMax(e) || e.code === "Space");

            function checkMax(e) {
                if (e.target.textContent.length === 2) {
                    console.log("CheckMax");
                    if (Number(e.target.textContent) === 25) {
                        if (Number(e.key) >= 6) {
                            return true
                        }
                    } else if (Number(e.target.textContent) > 25) {
                        return true;
                    }
                }
                return false;
            }
        }

        function getGuessValues() {
         //   selected.rGuess = rLabel.textContent;
          //  selected.gGuess = gLabel.textContent;
         //   selected.bGuess = bLabel.textContent;
        }
    }

}

function checkIfGameOver(color) {
    if (isGameOver) {
        bodyProps.classList.remove("bg-dark");
        bodyProps.classList.add("win");
    } else {
        color.classList.add("wrong");

    }
}

function changeLabel(r, g, b) {
    rLabel.textContent = r;
    gLabel.textContent = g;
    bLabel.textContent = b;
}

function checkMode() {
    currentMode === gameMode.EASY ? getWinner() : startHard();
}
function createTip() {
    let sentence;
    sentence = 'You need ';
    if (Number(rLabel.textContent) !== selected.r) {

        if (Number(rLabel.textContent) < selected.r) {
            sentence += 'more '
        } else if (Number(rLabel.textContent) > selected.r) {
            sentence += 'less ';
        }
        sentence += ' red '
    }
    if (Number(gLabel.textContent) !== selected.g) {
        if (Number(gLabel.textContent) < selected.g) {
            sentence += 'more '
        } else if (Number(gLabel.textContent) > selected.g) {
            sentence += 'less ';
        }
        sentence += ' green '
    }
    if (Number(bLabel.textContent) !== selected.b) {
        if (Number(bLabel.textContent) < selected.b) {
            sentence += 'more '
        } else if (Number(bLabel.textContent) > selected.b) {
            sentence += 'less ';
        }
        sentence += ' blue '
    }
    console.log(sentence);
    pTip.textContent = sentence;
}
/*
TODO: Add losing condition  to challenge player
 */