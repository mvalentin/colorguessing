 # Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v0.2.1] - 2020-03-27
### Added:
- Animation to color switching.

### Fixed:
- Container selected on hard staying after reset.

## [v0.2.0] - 2020-03-20
### Added:
- Hard Mode difficulty
- Tips for Hard Mode input
- Locked input on right answers
### Fixed:
- Documentation 


## [v0.1.0] - 2020-03-13
### Added: 
- Adds color to containers.
- Creates win condition
- Adds feedback so user know if they had won or made mistake.

## [v0.0.1] - 2020-03-11
 ### Added:
- Created Readme, Changelog and .gitignore file.
- Adds main app template.
- Adds random color generator


[v0.2.1]:https://gitlab.com/mvalentin/colorguessing/-/tags/v0.2.1
[v0.2.0]:https://gitlab.com/mvalentin/colorguessing/-/tags/v0.2.0
[v0.1.0]:https://gitlab.com/mvalentin/colorguessing/-/tags/v0.1.0
[v0.0.1]:https://gitlab.com/mvalentin/colorguessing/-/tags/v0.0.1
